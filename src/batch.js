/**
 * This module is intended to batch requests coming from multiple sources,
 * execute them in parallel and return the a list of results. This output
 * can be either some data (reponse body of an HTTP request for example) or
 * an error. It is up to the caller to handle errors according to its use cases.
 */

const got = require('got');
const R = require('ramda');

//--------------------------------------------------------
let sendBatch = async function(urls) {

  console.log('sending batch for %o', urls);
  let results = R.map(fetchAsync, urls);
  return Promise.all(results);
}

//--------------------------------------------------------
let fetchAsync = async function(url) {

  try {
    let response = await got(url);
    return {url: url, ok: true, data: JSON.parse(response.body)};
  } catch(error) {
    return {url: url, ok: false, error: error};
  }
}

module.exports.sendBatch = sendBatch;
