/**
 * Fetch the sunrise / sunset times for a list of coordinates on the map.
 * The requests are run in parallel, in a configurable batch size. Once
 * the first batch finishes, the next one is executed, up until all requests
 * are finished.
 *
 * Usage:
 *  e = new Executor(20, 5);
 *  e.asyncInit().then(() => console.log(e.sunrises));
 */

const R = require('ramda');

const { generateCoordinates } = require('./coordinates_generator');

const { sendBatch } = require('./batch');

const REMOTE_URL = 'https://api.sunrise-sunset.org/json';
const TIMEOUT = 1000;

class Executor {

  constructor(totalNumber, batchSize) {

    this.totalNumber = totalNumber;
    this.batchSize = batchSize;
    this.coordinates = this.initCoordinates();
    this.sunrises = [];
    this.earliestSunrise = undefined;
  }

  //--------------------------------------------------------
  async asyncInit() {

    let sunrises = [];
    let earliest = undefined;

    let batch = [];
    for(let index = 0; index < this.totalNumber; index++) {

      // build the list of urls to fetch
      let coord = this.coordinates[index];
      batch.push(Executor.buildURL(coord));

      if((index + 1) % this.batchSize === 0) {
        // send the batch
        const batchResult = await sendBatch(batch);
        const {data, failedUrls} = this.filter(batchResult);
        sunrises = sunrises.concat(data);
        earliest = this.retrieveEarliestSunrise(data);
        await this.wait(TIMEOUT);

        // reset the list of urls
        batch = [];
      }
    }

    this.sunrises = sunrises;
    if(!this.earliestSunrise) {
      this.earliestSunrise = earliest;
    } else {
      this.earliestSunrise = this.retrieveEarliestSunrise([this.earliestSunrise, earliest]);
    }
  }

  //--------------------------------------------------------
  async wait(timeout) {
    console.log('Waiting %s...', timeout);
    return new Promise((resolve, reject) => {
      setTimeout(resolve, timeout);
    });
  }

  //--------------------------------------------------------
  filter = (batchResult) => {
    console.log(batchResult);
    let objects = batchResult.filter((obj) => obj.ok === true);
    let errors = batchResult.filter((obj) => obj.ok === false);

    return {
      data: objects.map((obj) => obj.data.results),
      failedUrls: errors.map((error) => error.url)
    }
  }

  //--------------------------------------------------------
  // @doc Given a list of objects returned by the sunrise API,
  // return the one object containing the earliest sunrise.
  // In order to determine the required object, we must first
  // convert the time into seconds, such that we can correctly
  // compare two times (i.e. avoid compare two time strings
  // lexicographically).
  retrieveEarliestSunrise = (data) => {

    const sunriseVal = (elem) => R.prop('sunrise', elem);
    const timeToSeconds = R.curry((today, timeString) => {
      const year = today.getFullYear();

      // Cater for the 0-indexed month
      const month = today.getMonth() + 1;
      const day = today.getDate();

      const myDate = `${year}-${month}-${day} ${timeString}`
      const seconds = new Date(myDate).getTime();
      return seconds;
    });

    const dateToday = new Date();
    const mapFun = (date) => R.pipe(
      sunriseVal,
      timeToSeconds(date)
    );

    return R.reduceRight(R.minBy(mapFun(dateToday)), Infinity, data);
  }

  //--------------------------------------------------------
  initCoordinates = () => {
    return generateCoordinates(this.totalNumber);
  }

  //--------------------------------------------------------
  static buildURL = ({lat, long}) => {
    let url = new URL(REMOTE_URL);
    let qs = url.searchParams;
    qs.set('lat', lat);
    qs.set('lng', long);
    qs.set('date', 'today');
    return url.href;
  };
}

module.exports.Executor = Executor
