/**
 * @doc Module that generates a list of random coordinates
 * on the map of a specified size.
 */

const R = require('ramda');

const gen = () => {
  const getRandomFloat = (min, max) => Math.random() * (max - min) + min;

  let lat = getRandomFloat(-90, 90);
  let long = getRandomFloat(-180, 180);

  return { lat, long };
};

const generateCoordinates = function(count) {
  return R.map(gen, R.range(0, count));
}

module.exports.generateCoordinates = generateCoordinates;
